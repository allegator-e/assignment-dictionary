%include "colon.inc"
%include "words.inc"
%include "lib.inc"

%define size 255

global _start

extern find_word

section .rodata
empty_exc: db "По заданному ключу нет значения", 0
length_exc: db "Ключ превышает 255 символов", 0

section .text

%define STDOUT 1
%define STDERR 2

_start:
	sub rsp, size
	mov rsi, size
	mov rdi, rsp
	call read_word
	test rax, rax
	je .err
	
	mov rsi, prvs
	mov rdi, rax
	call find_word
	test rax, rax
	je .empty
	
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	inc rax
	add rdi, rax
	push rsi
	mov r9, STDOUT
	call print_string
	pop rsi
	jmp .end
	
.err:
	mov rdi, length_exc
	jmp .err_end
.empty:
	mov rdi, empty_exc
.err_end:
	mov r9, STDERR
	call print_string	
.end:
	call print_newline
	add rsp, size
	jmp _start
