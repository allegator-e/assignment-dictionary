section .text
 
 global exit
 global string_length
 global print_string
 global print_char
 global print_newline
 global print_uint
 global print_int
 global string_equals
 global read_char
 global read_word
 global parse_uint
 global parse_int
 global string_copy
 
 
 
section .text

%define EXIT 60
%define READ 0
%define WRITE 1
%define STDIN 0
%define STDOUT 1
%define NEW_LINE 0xA
%define TAB 0x9
%define SIZE_DECIMAL_NUMBER 21
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, WRITE
    mov rdi, r9
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, rsp
    sub rsp, SIZE_DECIMAL_NUMBER
    dec rcx
    mov [rcx], byte 0
    mov r10, 10
  .next_number:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rcx
    mov [rcx], dl
    test rax, rax
    jne .next_number
    mov rdi, rcx
    push rdi
    call print_string
    pop rdi
    add rsp, SIZE_DECIMAL_NUMBER
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r10, r10
    mov rax, 1
  .loop:
    mov r10b, [rdi+rcx]
    cmp r10b, [rsi+rcx]
    jne .not_equals
    test r10, r10
    je .end
    inc rcx
    jmp .loop
  .not_equals:
    xor rax, rax
  .end:
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
    dec rsi
    xor r9, r9
    xor r10, r10
  .next_char:
  
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    
    cmp rax, TAB
    je .check
    cmp rax, NEW_LINE
    je .check
    cmp rax, ' '
    je .check
    mov r9, 1
    
    test rax, rax
    je .check
    cmp r10, rsi
    je .err
    
    mov [rdi+r10], rax
    inc r10
    jmp .next_char
    
  .check:
    test r9, r9
    jne .end_word
    jmp .next_char
    
  .end_word:
    mov [rdi+r10], byte 0
    mov rax, rdi
    mov rdx, r10
    jmp .end
  .err:
    xor rax, rax
  .end:
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 10
  .next_number:
    mov r9b, [rdi+r8]
    cmp r9b, '9'
    ja .end
    sub r9b, '0'
    jb .end
    mul r10
    add rax, r9
    inc r8
    jmp .next_number
  .end:
    mov rdx, r8
    ret
    
    
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    je .neg
    jne parse_uint
  .neg:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
  .next_char:
    cmp rax, rdx
    je .err
    mov r9b, [rdi+rax]
    mov [rsi+rax], r9b
    test r8b, r8b
    je .end
    inc rax
    jmp .next_char
  .err:
    xor rax, rax
  .end:
    ret
