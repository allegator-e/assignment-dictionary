.PHONY: all clean
ASM=nasm -f elf64
LD=ld

all: dictionary

lib.o: lib.asm
	$(ASM) -o $@ $<

dict.o: dict.asm lib.o
	$(ASM) -o $@ $<

main.o: main.asm dict.o lib.o words.inc colon.inc lib.inc dict.inc
	$(ASM) -o $@ $<

dictionary: dict.o lib.o main.o
	$(LD) -o $@ $^

clean:
	rm lib.o dict.o main.o dictionary


