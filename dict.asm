extern string_equals

global find_word

;rsi - указатель словарь
;rdi - указатель на ключ

find_word:
    .loop:
    	test rsi, rsi
    	je .end
    	push rsi
    	add rsi, 8
    	call string_equals
    	pop rsi
    	test rax, rax
    	jne .end
    	mov rsi, [rsi]
    	jmp .loop
    	
    .end:
    	mov rax, rsi
    	ret
